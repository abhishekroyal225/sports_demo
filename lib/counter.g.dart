// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'counter.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SportsApi on SportsApiBase, Store {
  final _$countryListAtom = Atom(name: 'SportsApiBase.countryList');

  @override
  List<Country> get countryList {
    _$countryListAtom.reportRead();
    return super.countryList;
  }

  @override
  set countryList(List<Country> value) {
    _$countryListAtom.reportWrite(value, super.countryList, () {
      super.countryList = value;
    });
  }

  final _$getDataAsyncAction = AsyncAction('SportsApiBase.getData');

  @override
  Future getData(String _countryname) {
    return _$getDataAsyncAction.run(() => super.getData(_countryname));
  }

  @override
  String toString() {
    return '''
countryList: ${countryList}
    ''';
  }
}
