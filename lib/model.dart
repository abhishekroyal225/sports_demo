class Country {
  String strSport     = '';
  String strLeague    = '';
  String strFacebook  = '';
  String strTwitter   = '';
  String strYoutube   = '';
  String strLogo      = '';
  String strPoster    = '';

  Country({
    this.strSport,
    this.strLeague,
    this.strFacebook,
    this.strTwitter,
    this.strYoutube,
    this.strLogo,
    this.strPoster,
  });

  factory Country.fromJSON(Map<String, dynamic> parsedJSON) {
    return Country(
      strSport    : parsedJSON['strSport'],
      strLeague   : parsedJSON['strLeague'],
      strFacebook : parsedJSON['strFacebook'],
      strTwitter  : parsedJSON['strTwitter'],
      strYoutube  : parsedJSON['strYoutube'],
      strLogo     : parsedJSON['strLogo'],
      strPoster   : parsedJSON['strPoster'],
    );
  }
}
