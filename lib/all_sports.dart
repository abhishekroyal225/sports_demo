import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:sports_demo/counter.dart';

class AllSports extends StatefulWidget {
  String countryName;
  AllSports({
    this.countryName,
  });
  @override
  _AllSportsState createState() => _AllSportsState();
}

class _AllSportsState extends State<AllSports> {
  SportsApi sportsApi = SportsApi();
  TextEditingController _textEditingController = TextEditingController();
  double _height = 0, _width = 0;
  @override
  void initState() {
    super.initState();
    sportsApi.getData(widget.countryName);
  }
  
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.red,
          title: Text(
            widget.countryName,
            style: TextStyle(color: Colors.white),
          ),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () => Navigator.of(context).pop()),
        ),
        body: Observer(builder: (context) {
          return SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Container(
                  height: _height * 0.045,
                  margin: EdgeInsets.only(
                    left: _width * 0.03,
                    right: _width * 0.03,
                    top: _height * 0.02,
                  ),
                  padding: EdgeInsets.only(
                    left: _width * 0.04,
                    top: 8,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.3),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: TextField(
                    controller: _textEditingController,
                    onChanged: (value) {
                      if (value.isNotEmpty) {
                        // Search API Call
                      } else {
                        // If search is clear then all league api is call for refreh data;
                      }
                    },
                    decoration: InputDecoration(
                      hintText: 'Search Leagues...',
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(
                  height: _height * 0.03,
                ),

              ],
            ),
          );
        }));
  }
}
