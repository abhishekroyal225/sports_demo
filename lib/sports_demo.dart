import 'package:flutter/material.dart';
import 'package:sports_demo/all_sports.dart';

class SportsDemo extends StatefulWidget {
  @override
  _SportsDemoState createState() => _SportsDemoState();
}

class _SportsDemoState extends State<SportsDemo> {
  double _height, _width;
  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    List<String> _countriesList = [
      'India',
      'United States',
      'Australia',
      'China',
      'Argentina',
      'England'
    ];
    return Scaffold(
      backgroundColor: Colors.red,
      body: Container(
        margin: EdgeInsets.only(
          top: _height * 0.16,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Center(
                child: Text(
                  'The Sports DB',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: _height * 0.04,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: _countriesList.length,
                itemBuilder: ((BuildContext context, int index) {
                  return InkWell(
                    onTap: () => Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              AllSports(countryName: _countriesList[index]),
                        ),
                        (route) => true),
                    child: Container(
                      height: _height * 0.07,
                      width: _width,
                      margin: EdgeInsets.only(
                        left: _width * 0.02,
                        right: _width * 0.02,
                        bottom: _height * 0.02,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: _width * 0.03),
                            child: Text(
                              _countriesList[index],
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: _width * 0.02),
                            child: Icon(
                              Icons.arrow_forward,
                              size: 30,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
