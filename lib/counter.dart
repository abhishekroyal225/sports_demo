import 'package:dio/dio.dart';
import 'package:mobx/mobx.dart';
import 'package:sports_demo/model.dart';

part 'counter.g.dart';

class SportsApi = SportsApiBase with _$SportsApi;

abstract class SportsApiBase with Store {
  @observable
  List<Country> countryList = List<Country>();

  @action
  getData(String _countryname) async {
    try {
      Response response = await Dio().get(
          'https://www.thesportsdb.com/api/v1/json/1/search_all_leagues.php?c=$_countryname');
      countryList = response.data['countrys'].map<Country>((jsonData) => Country.fromJSON(jsonData)).toList();
    } catch (e) {
      print(e);
    }
  }
}
